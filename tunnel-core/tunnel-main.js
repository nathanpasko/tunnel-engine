// Tunnel Engine Core

const position = {
  inner: 'INNER',
  focused: 'FOCUSED',
  outer: 'OUTER',
};
const sides = ['top', 'right', 'bottom', 'left'];

var cartridge;
var sources = [];
var currentLayer = 1;
var controlContext;
var controlOsc;

var frame;
var activeLayers = [];

// Initialization

function initNav() {
  // Grab nav element
  const nav = document.querySelector('header nav');
  // For each link in nav array
  cartridge.info.nav.forEach((link) => {
    // Add a link element to the nav
    const elem = getNavElement(link);
    nav.appendChild(elem);
  });
}
function initHeader() {
  // If cartridge has labels
  if (cartridge.info.labels) {
    setTimeout(() => {
      document.getElementById('corner-logo').classList.add('active');
    }, 1250);
    document.getElementById('startup-logo').classList.add('extended');
    setTimeout(clearStartupLogo, 2500);
    const title = document.getElementById('title');
    title.textContent = cartridge.info.title;
    if (cartridge.info.nav.length > 0) initNav();
    setTimeout(() => {
      document.getElementById('main-header').classList.add('active');
    }, 1500);
  }
  // If cartridge doesn't have labels
  else {
    setTimeout(() => {
      document.getElementById('corner-logo').classList.add('active');
    }, 600);
    setTimeout(clearStartupLogo, 1250);
    document.getElementById('main-header').remove();
  }
}
function initFrame() {
  // Grab frame
  frame = document.getElementById('frame');
  // Make sure cartridge has at least 3 layers
  while (cartridge.layers.length < 3) {
    addBlankLayer();
  }
  // Create first three layers
  createLayer(cartridge.layers[2], position.inner, 2);
  createLayer(cartridge.layers[1], position.focused, 1);
  createLayer(cartridge.layers[0], position.outer, 0);
  // Update background to initial color
  updateBackground();
}
function initButtons() {
  // Set up event listeners for main control buttons
  document.getElementById('inward-btn').addEventListener('click', () => {
    moveLayer(1);
  });
  document.getElementById('outward-btn').addEventListener('click', () => {
    moveLayer(-1);
  });
}
function initIOS(val) {
  // Grab message element for iOS
  var iosElem = document.getElementById('ios');
  // If client device is iOS
  if (val) {
    // Activate message
    iosElem.classList.add('active');
    // Get ready to clear message
    window.setTimeout(() => {
      iosElem.classList.remove('active');
    }, 2000);
    window.setTimeout(() => {
      iosElem.remove();
    }, 5000);
  }
  // if client device is not iOS
  else {
    // Immediately clear message
    iosElem.remove();
  }
}
function loadCartridge(cart) {
  // Grab the cartridge data
  cartridge = cart;
  // Set document title to cartridge title
  document.title = cartridge.info.title;
}
function init() {
  // Check for iOS & initialize if necessary
  initIOS(iOS());

  // Check whether a cartridge is loaded
  if (detectCartridge()) {
    // Fully initialize & play cartridge
    initHeader();
    initFrame();
    initButtons();
  } else {
    // No cartridge
    setTimeout(clearStartupLogo, 3500);
    // Show No Cartridge message
    document.getElementById('no-cart').classList.add('active');
    // Remove controls
    document.getElementById('controls').remove();
  }
}

// Main controls

function getNavElement(data) {
  var elem = document.createElement('a');
  elem.textContent = data.text;
  elem.setAttribute('href', data.href);
  elem.classList.add('prevent-select');
  return elem;
}
function getControlWidgets() {
  // create control widgets div with 4 children
  const widgets = document.createElement('div');
  widgets.classList.add('widgets');
  for (var i = 0; i < 4; i++) {
    const w = document.createElement('div');
    widgets.appendChild(w);
  }
  return widgets;
}
function getControlLayerElem() {
  const elem = document.createElement('div');
  elem.classList.add('control');
  // control layer setup
  const cross1 = document.createElement('div');
  cross1.classList.add('cross');
  elem.appendChild(cross1);
  const widgets = getControlWidgets();
  elem.appendChild(widgets);
  const cross2 = document.createElement('div');
  cross2.classList.add('cross');
  elem.appendChild(cross2);
  const logo = document.createElement('img');
  logo.src = 'logo-sm.svg';
  elem.appendChild(logo);
  return elem;
}
function createSlide(layer, index, control = false) {
  var slide = document.createElement('div');
  slide.classList.add('slide');
  slide.classList.add(sides[index]);
  /*
  if (layer.background) 
    slide.style.backgroundColor = layer.background;*/
  if (layer.images && layer.images[index].length > 0) {
    slide.style.backgroundImage = `url(${layer.images[index]})`;
  }
  if (control) {
    const elem = getControlLayerElem();
    slide.appendChild(elem);
  }

  return slide;
}
function addAudioSource(layerId, url) {
  // audio context fallback for legacy browsers
  const AudioContext = window.AudioContext || window.webkitAudioContext;
  // Create an audio context
  const audioContext = new AudioContext();
  // create buffer type audio source
  var source = audioContext.createBufferSource();
  // Load the buffer
  // create Request
  var request = new XMLHttpRequest();
  // open the request -- type get, async true
  request.open('GET', url, true);
  //webaudio parameters
  request.responseType = 'arraybuffer';
  // once the request is completed...
  request.onload = function () {
    audioContext.decodeAudioData(
      request.response,
      function (res) {
        // set up and play source after buffer has loaded
        source.buffer = res;
        source.loop = true;
        source.start(0);
      },
      function () {
        console.error('The request failed.');
      }
    );
  };
  // send request
  request.send();
  // store audio context & source
  var s = {
    id: layerId,
    audioContext: audioContext,
    source: source,
  };
  sources.push(s);
}
function createLayer(data, position, index) {
  // Check whether this is the control layer
  const control = index == 0 ? true : false;
  // Apply layer data to new HTML element
  var layer = document.createElement('div');
  layer.id = data.id;
  layer.classList.add('layer');
  layer.dataset.position = position;
  // Set up audio if necessary
  if (data.audio) {
    addAudioSource(data.id, data.audio);
  }
  // Create four slides
  for (var i = 0; i < 4; i++) {
    var slide = createSlide(data, i, control);
    layer.appendChild(slide);
  }
  // Report layer to array & DOM
  frame.appendChild(layer);
  activeLayers.push(layer);
}
function removeLayer(position) {
  // try to find active layer to remove
  var toRemove = activeLayers.findIndex(
    (layer) => layer.dataset.position == position
  );
  if (toRemove >= 0) {
    // remove from script and DOM
    var removedElements = activeLayers.splice(toRemove, 1);
    document.getElementById(removedElements[0].id).remove();
    removeSourceForId(removedElements[0].id);
    return removedElements;
  }
}
function removeSourceForId(id) {
  // try to find audio source
  var i = sources.findIndex((s) => s.id == id);
  // remove source if found
  if (i >= 0) {
    sources.splice(i, 1);
  }
}
function moveLayer(val) {
  if (currentLayer + val >= 0 && currentLayer + val < cartridge.layers.length) {
    currentLayer = currentLayer + val;
    // move inward
    if (val > 0) {
      var removed = removeLayer(position.outer);
      // set data sets via activeLayers
      // find x
      var xIndex = activeLayers.findIndex(
        (l) => l.dataset.position == position.inner
      );
      // find y
      var yIndex = activeLayers.findIndex(
        (l) => l.dataset.position == position.focused
      );
      // set x
      if (xIndex >= 0) {
        activeLayers[xIndex].dataset.position = position.focused;
        updateBackground();
        tapAudio(activeLayers[xIndex], false);
      }
      // set y
      if (yIndex >= 0) {
        activeLayers[yIndex].dataset.position = position.outer;
        tapAudio(activeLayers[yIndex], true, currentLayer == 1);
      }
      // create next layer
      if (cartridge.layers.length > currentLayer + 1)
        createLayer(
          cartridge.layers[currentLayer + 1],
          position.inner,
          currentLayer + 1
        );
    }
    // move outward
    else {
      var removed = removeLayer(position.inner);
      // set data sets via activeLayers
      // find x
      var xIndex = activeLayers.findIndex(
        (l) => l.dataset.position == position.outer
      );
      // find y
      var yIndex = activeLayers.findIndex(
        (l) => l.dataset.position == position.focused
      );
      // set x
      if (xIndex >= 0) {
        activeLayers[xIndex].dataset.position = position.focused;
        updateBackground();
        tapAudio(activeLayers[xIndex], false, currentLayer == 0);
      }
      // set y
      if (yIndex >= 0) {
        activeLayers[yIndex].dataset.position = position.inner;
        tapAudio(activeLayers[yIndex], true);
      }
      // create next layer
      if (currentLayer > 0)
        createLayer(
          cartridge.layers[currentLayer - 1],
          position.outer,
          currentLayer - 1
        );
    }
  }
}
function setControlOscillator(val) {
  // control layer start
  if (val) {
    // set up control layer audio context & oscillator
    const AudioContext = window.AudioContext || window.webkitAudioContext;
    controlContext = new AudioContext();
    controlOsc = controlContext.createOscillator();
    // frequency of planet Neptune
    controlOsc.frequency.setValueAtTime(211.44, controlContext.currentTime);
    controlOsc.connect(controlContext.destination);
    // dampen the tone with a little bit here using a gain node?
    // start oscillator
    controlOsc.start();
  }
  // control layer stop
  else {
    controlContext.suspend();
    controlOsc.stop();
    controlOsc.disconnect(controlContext.destination);
    controlOsc = null;
  }
}
function tapAudio(elem, stop = false, control = false) {
  // try to find audio source with ID
  var i = sources.findIndex((s) => s.id == parseInt(elem.id));
  // if we found the source
  if (i >= 0) {
    // stop audio
    if (stop) {
      sources[i].source.disconnect();
      sources[i].audioContext.suspend();
      // stop control layer tone
      if (control == true) {
        setControlOscillator(false);
      }
    }
    // start audio
    else {
      sources[i].source.connect(sources[i].audioContext.destination);
      sources[i].audioContext.resume();
      // start control layer tone
      if (control == true) {
        setControlOscillator(true);
      }
    }
  }
}

// Utility

function iOS() {
  // return true if client device is running iOS
  return (
    [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod',
    ].includes(navigator.platform) ||
    (navigator.userAgent.includes('Mac') && 'ontouchend' in document)
  );
}
function detectCartridge() {
  var val = false;
  // Look for javascript cartridge
  if (typeof info != 'undefined') {
    val = true;
    const cart = {
      info: info,
      layers: layers,
    };
    loadCartridge(cart);
  }
  // If that didn't work, try JSON
  else {
    const cartridgeElem = document.getElementById('cartridge');
    if (cartridgeElem) {
      const j = JSON.parse(cartridgeElem.innerText);
      if (typeof j.info != 'undefined') {
        val = true;
        const cart = {
          info: j.info,
          layers: j.layers,
        };
        loadCartridge(cart);
      }
    }
  }
  return val;
}
function clearStartupLogo() {
  document.getElementById('startup-logo').remove();
}
function addBlankLayer() {
  cartridge.layers.push({
    id: Math.random() * 100,
    name: '',
    images: ['', '', '', ''],
  });
}
function setLayerPosition(element, val) {
  element.dataset.position = val;
}

// Update

function updateBackground() {
  // If layer has background setting
  if (cartridge.layers[currentLayer].background != undefined) {
    // set frame background
    frame.style.background = cartridge.layers[currentLayer].background;
  } else {
    // otherwise clear the frame background
    frame.style.background = 'unset';
  }
}

init();
