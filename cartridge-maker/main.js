const navUl = document.querySelector('#form section#nav ul');
const linkEdit = document.getElementById('link-edit')
const linkText = document.getElementById('new-link-text');
const linkUrl = document.getElementById('new-link-url');
const layerEdit = document.getElementById('layer-edit');
const currentLinkSpan = document.getElementById('current-link');
const currentIndexSpan = document.getElementById('current-index');
const layerUl = document.querySelector('#form section#layers ul');
const layerId = document.getElementById('layer-id');
const audioPath = document.getElementById('audio');
const colorPicker = document.getElementById('color-picker');

var currentIndex = -1;
var currentLink = -1;
var info = {
    title: '',
    nav: [],
    labels: true
};
var layers = [];

function download(content, fileName, contentType) {
    var a = document.createElement('a');
    var file = new Blob([content], {
        type: contentType
    });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}

function getLinkItem(text, href, index) {
    var elem = document.createElement('li');
    var pText = document.createElement('p');
    pText.textContent = text.length > 0 ? text : '---';
    var button = document.createElement('button');
    button.classList.add('delete');
    button.dataset.href = href;
    button.setAttribute('aria-label', `Delete ${text} Link`);
    button.textContent = 'X';
    button.onclick = e => clickLinkDelete(e);
    pText.appendChild(button);
    elem.appendChild(pText);
    var edit = document.createElement('button');
    edit.classList.add('edit');
    edit.dataset.linkIndex = index;
    edit.setAttribute('aria-label', `Edit Link ${text}`);
    edit.textContent = 'Edit';
    edit.onclick = e => clickLinkEdit(e);
    pText.appendChild(edit);
    var pHref = document.createElement('p');
    var a = document.createElement('a');
    a.setAttribute('href', href);
    a.textContent = href;
    pHref.appendChild(a);
    elem.appendChild(pHref);
    return elem;
}

function getLayerItem(layer, index) {
    console.log('get layer item', index, layer);
    var elem = document.createElement('li');
    var pText = document.createElement('p');
    var layerId = `${layer.id}` || '---';
    pText.textContent = `[${index}] ID: ${layerId}`;
    var button = document.createElement('button');
    button.classList.add('delete');
    button.dataset.layerIndex = index;
    button.setAttribute('aria-label', `Delete Layer ${layer.id}`);
    button.textContent = 'X';
    button.onclick = e => clickLayerDelete(e);
    pText.appendChild(button);
    var edit = document.createElement('button');
    edit.classList.add('edit');
    edit.dataset.layerIndex = index;
    edit.setAttribute('aria-label', `Edit Layer ${layer.id}`);
    edit.textContent = 'Edit';
    edit.onclick = e => clickLayerEdit(e);
    pText.appendChild(edit);
    if (index == 0) {
        var control = document.createElement('span');
        control.classList.add('control');
        control.textContent = 'control';
        pText.appendChild(control);
    }
    elem.appendChild(pText);
    return elem;
}

function getEmptyMessage(section = 'nav') {
    console.log('empty');
    var p = document.createElement('p');
    if (section == 'nav')
        p.innerHTML = `<strong>Empty.</strong> Add links to populate cartridge nav.`;
    else if (section == 'layers')
        p.innerHTML = `<strong>Empty.</strong> Add data for each layer in cartridge.`;
    return p;
}

function clearValue(input) {
    input.value = '';
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function setCurrentLink(val) {
    currentLink = val;
    if (currentLink >= 0) {
        currentLinkSpan.textContent = `[${val}]`;
    } else {
        currentLinkSpan.textContent = '';
    }
}

function setCurrentIndex(val) {
    currentIndex = val;
    if (currentIndex >= 0) {
        currentIndexSpan.textContent = `[${val}]`;
    } else {
        currentIndexSpan.textContent = '';
    }
}

function updateNavDisplay() {
    removeAllChildNodes(navUl);
    if (info.nav.length < 1) {
        var li = getEmptyMessage();
        navUl.appendChild(li);
    } else {
        for (var i = 0; i < info.nav.length; i++) {
            var li = getLinkItem(info.nav[i].text, info.nav[i].href, i);
            navUl.appendChild(li);
        }
    }
}

function removeLink(href) {
    const index = info.nav.findIndex(l => l.href == href);
    if (index >= 0) {
        info.nav.splice(index, 1);
        updateNavDisplay();
    } else {
        console.log('ERROR - link not found in array');
    }
}

function updateLayerDisplay() {
    console.log('update layer display', `${layers.length}ct`);
    removeAllChildNodes(layerUl);
    if (layers.length < 1) {
        var li = getEmptyMessage('layers');
        layerUl.appendChild(li);
    } else {
        for (var i = 0; i < layers.length; i++) {
            var li = getLayerItem(layers[i], i);
            layerUl.appendChild(li);
        }
    }
}

function sanitizeLinkData() {
    var text = linkText.value || '';
    var href = linkUrl.value || '';
    var linkData = {
        text,
        href
    };
    return linkData;
}

function sanitizeLayerData() {
    var id = layerId.value || '';
    var audio = audioPath.value || '';
    var images = [];
    for (var i = 0; i < 4; i++) {
        const val = document.getElementById(`image${i}`).value || '';
        images.push(val);
    }
    var layerData = {
        id,
        audio,
        images
    };
    var transparency = document.querySelector('input[name="transparency"]:checked');
    if (transparency.value == 'color') {
        layerData['background'] = colorPicker.value;
    }
    return layerData;
}

function setLinkEditor() {
    if (currentLink >= 0) {
        const link = info.nav[currentLink];
        linkText.value = link.text;
        linkUrl.value = link.href;
        linkEdit.classList.add('active');
    } else {

        linkEdit.classList.remove('active');
    }
}

function setLayerEditor() {
    if (currentIndex >= 0) {
        const layer = layers[currentIndex];
        layerId.value = layer.id;
        if (layer.audio)
            audioPath.value = layer.audio;
        if (layer.background)
            colorPicker.value = layer.background;
        for (var i = 0; i < 4; i++) {
            if (layer.images && layer.images[i])
                document.getElementById(`image${i}`).value = layer.images[i];
        }
        layerEdit.classList.add('active');
    } else {
        clearValue(layerId);
        clearValue(audioPath);
        document.getElementById('transparent').checked = true;
        document.getElementById('choose-color').checked = false;
        colorPicker.value = '#000000';
        for (var i = 0; i < 4; i++) {
            document.getElementById(`image${i}`).value = '';
        }
        layerEdit.classList.remove('active');
    }
}

function clickLinkSave() {
    var linkData = sanitizeLinkData();
    info.nav[currentLink] = linkData;
    setCurrentLink(-1);
    setLinkEditor();
    updateNavDisplay();
}

function clickLayerSave() {
    var layerData = sanitizeLayerData();
    console.log(layerData);
    layers[currentIndex] = layerData;
    setCurrentIndex(-1);
    setLayerEditor();
    updateLayerDisplay();
}

function clickLinkDelete(e) {
    const href = e.target.dataset.href;
    console.log(href);
    removeLink(href);
}

function clickNewLink() {
    info.nav.push({
        text: '',
        href: ''
    });
    updateNavDisplay();
}

function clickLinkEdit(e) {
    var index = e.target.dataset.linkIndex;
    console.log('edit link @', index);
    setCurrentLink(index);
    setLinkEditor();
}
/*function clickLinkAdd() {
  var link = {
    text: linkText.value,
    href: linkUrl.value
  };
  info.nav.push(link);
  updateNavDisplay();
  console.log('add', link);
  clearValue(linkText);
  clearValue(linkUrl);
}*/
function clickLinkCancel() {
    setCurrentLink(-1);
    setLinkEditor();
}

function clickNewLayer() {
    layers.push({
        id: ''
    });
    updateLayerDisplay();
}

function clickLayerDelete(e) {
    console.log('layer delete on index', e.target.dataset.layerIndex);
    layers.splice(e.target.dataset.layerIndex, 1);
    updateLayerDisplay();
}

function clickLayerEdit(e) {
    var index = e.target.dataset.layerIndex;
    console.log('edit layer @', index);
    setCurrentIndex(index);
    setLayerEditor();
}

function clickLayerCancel() {
    setCurrentIndex(-1);
    setLayerEditor();
}

function clickSubmit() {
    const cartridge = {
        info,
        layers
    };
    console.log('cart', cartridge);
    const jsonCartridge = JSON.stringify(cartridge);
    console.log(jsonCartridge);
    download(jsonCartridge, 'jsoncart.json', 'application/json');
}

updateNavDisplay();
updateLayerDisplay();