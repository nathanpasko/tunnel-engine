# Tunnel

_A temporally-unopinionated audio/video engine for the web._

Create, trade, remix, and share Tunnel cartridges. Each cartridge is composed of layers of sound, images, and color. The Tunnel engine is a new format designed to let the audience control the timing and structure of interactive artworks. Everything is open source and responsive.

**Tunnel HQ**
[https://tunnelengine.netlify.app](https://tunnelengine.netlify.app)

## What's in the repo

This repository contains the Tunnel engine and the Cartridge Maker tool.

### 📁 tunnel/tunnel-core/

The **Tunnel** engine with no cartridge loaded. This is the basis for any cartridge. Add a data file and assets to complete the package.

### 📁 tunnel/cartridge-maker/

The **Cartridge Maker** tool. Open **index.html** in your browser to use a simple cartridge creation assistant.

## Quick Start 

**For people comfortable with HTML and JavaScript.** You can also [read the full instructions to make a cartridge in the User's Manual](https://tunnelengine.netlify.app/manual-making-a-tunnel-cartridge).

1. Create a directory called **cart**.

2. Copy Tunnel into **cart**.

3. Open **index.html** in browser to see empty message.

4. Create **cartridge.js** in **cart**.

5. Write `info` object—see **Info Settings** in [Cartridge Data](https://tunnelengine.netlify.app/manual-cartridge-data).

6. Write `layers` array—see **Layer Settings** in [Cartridge Data](https://tunnelengine.netlify.app//manual-cartridge-data).

7. Embed **cartridge.js** into **index.html**.

8. Refresh **index.html** in browser to see loaded cartridge.

9. Add image and sound assets to directory and metadata to layers.